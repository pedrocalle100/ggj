﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour {

    public GameObject currentObject;

    public Transform boxPosition;

    public bool isGrab;

    public GameObject player;

    public Animator anim;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetKeyDown(KeyCode.E))
        {
            if (currentObject == null)
                return;

            if (!isGrab)
                GrabObject();
            else
                ReleaseObject();
        }

        if (isGrab == true)
        {
            currentObject.transform.position = boxPosition.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "prop"&&!isGrab)
        {
            currentObject = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {   
        if(collision.gameObject.GetInstanceID() == currentObject.GetInstanceID() && !isGrab)
            currentObject = null;
    }


        public void GrabObject()
    {
        anim.SetBool("Lift", true);
        isGrab = true;
        currentObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        player.GetComponent<Rigidbody2D>().mass += currentObject.GetComponent<PropBehaviour>().weight;
        //currentObject.transform.SetParent(transform,true);
        //currentObject.transform.position = transform.position;
    }

    public void ReleaseObject()
    {
        anim.SetBool("Lift", false);
        isGrab = false;
        currentObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        currentObject.transform.position = transform.position;
        player.GetComponent<Rigidbody2D>().mass -= currentObject.GetComponent<PropBehaviour>().weight;
        //currentObject.transform.SetParent(null);
    }
}
