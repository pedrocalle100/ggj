﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    [SerializeField]
    private Rigidbody2D rb;

    [SerializeField]
    private float hSpeed;

    [SerializeField]
    private float vSpeed;

    [SerializeField]
    private float limitSpeed;

    [SerializeField]
    private bool isGrounded = true;

    [SerializeField]
    private GameObject ground;

    public float weight;

    private Transform currentRotation;

    [SerializeField]
    private Animator anim;

    private bool isFacingRight = false;

    private int horizontal = 1;

    // Use this for initialization
    void Start()
    {
        isFacingRight = true;
        gameObject.GetComponent<Rigidbody2D>().mass = weight;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        

        if (!Input.anyKey)
            anim.SetBool("Run", false);
		if(Input.GetKey(KeyCode.D) && CheckSpeed())
        {
            rb.AddForce((Vector2.right * hSpeed),ForceMode2D.Force);
            anim.SetBool("Run", true);
            horizontal = 1;
            Flip(horizontal);
        }
        if (Input.GetKey(KeyCode.A)&&CheckSpeed())
        {
            rb.AddForce((Vector2.right * -hSpeed),ForceMode2D.Force);
            anim.SetBool("Run", true);
            horizontal = -1;
            Flip(horizontal);
            
        }
        if (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
        {
            //rb.velocity = Vector2.zero;
            KillMovementH();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded == true)
            {
                rb.AddForce(Vector2.up * vSpeed, ForceMode2D.Impulse);
                isGrounded = false;
            }
            else
            {
                return;
            }
        }

        gameObject.transform.rotation = ground.transform.rotation;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            isGrounded = true;
        }   
    }

    bool CheckSpeed()
    {
        if (rb.velocity.magnitude < 20f)
        {
            return true;
        }
        return false;

    }

    public void Flip(float horizontal)
    {
        if(horizontal > 0 && !isFacingRight || horizontal < 0 && isFacingRight)
        {
            isFacingRight = !isFacingRight;
            Vector3 playerScale = transform.localScale;
            playerScale.x *= -1;
            transform.localScale = playerScale;
        }
    }

    void KillMovementH()
    {
        Vector2 tempVec = new Vector2(0f, rb.velocity.y);
        rb.velocity = tempVec;
    }
}
