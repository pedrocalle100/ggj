﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetObject : MonoBehaviour
{
    public GameObject currentObject;

    public bool isSet;
    public Transform ground;
    public Animator anim;


    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {

            if (currentObject == null)
                return;

            if (!currentObject.GetComponent<PropBehaviour>().isSet)
                SetGrounded();
            else
                ReleaseObject();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "prop")
        {
            currentObject = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetInstanceID() == currentObject.GetInstanceID() && collision.tag != "balance" && collision.tag == "prop")
        {
            currentObject = null;
        }
            
    }

    public void SetGrounded()
    {
        anim.SetBool("Hammer", true);
        isSet = true;
        currentObject.GetComponent<PropBehaviour>().isSet = true;
        currentObject.transform.SetParent(ground,true);
        currentObject.GetComponent<BoxCollider2D>().isTrigger = true;
        currentObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        //currentObject.GetComponent<Rigidbody2D>().simulated = false;
    }

    public void ReleaseObject()
    {
        
        isSet = false;
        currentObject.GetComponent<PropBehaviour>().isSet = false;
        currentObject.transform.SetParent(null);
        currentObject.GetComponent<BoxCollider2D>().isTrigger = false;
        currentObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        //currentObject.GetComponent<Rigidbody2D>().simulated = true;
    }
}
