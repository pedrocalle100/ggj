﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balance : MonoBehaviour {

    public float myWeight;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "prop")
        {
            //Se accede al script del objecto para sacar peso y mandarlo al manager
            myWeight += other.GetComponent<PropBehaviour>().weight;
        }
        if (other.tag == "Player")
            myWeight += other.GetComponent<Movement>().weight;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "prop")
            myWeight -= other.GetComponent<PropBehaviour>().weight;
        if (other.tag == "Player")
            myWeight -= other.GetComponent<Movement>().weight;
    }
}
