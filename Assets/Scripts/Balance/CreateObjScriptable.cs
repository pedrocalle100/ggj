﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateObjScriptable {
    [MenuItem("Assets/CreateGGJ/Prop")]
    public static void CreateAsset()
    {
        Props asset = ScriptableObject.CreateInstance<Props>();

        AssetDatabase.CreateAsset(asset, "Assets/NewProp.asset");
        AssetDatabase.SaveAssets();
    }
	
}
