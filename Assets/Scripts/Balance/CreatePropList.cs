﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreatePropList : MonoBehaviour {
    [MenuItem("Assets/CreateGGJ/PropList")]
    public static void CreateAsset()
    {
        PropList asset = ScriptableObject.CreateInstance<PropList>();

        AssetDatabase.CreateAsset(asset, "Assets/NewPropList.asset");
        AssetDatabase.SaveAssets();
    }
	
}
