﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

    public GameObject floor;
    public BoxCollider2D rightBalance, leftBalance;
    public float weightRight, weightLeft, damper;
    public GameObject blankProp;
    public PropList propList;
    public GameObject reaper, bat, witch;
    public float callEnemy = 0f;
    public float enemyTimer;
    public float difficulty;

    public Transform spawn;
    private bool dirRight;
    public float spawnSpeed, spawnWidth, spawnHeight;

    public bool gameOver = false;
    

    public void FixedUpdate()
    {
        CalcRotation(weightRight, weightLeft);
        if (floor.transform.rotation.z > 0.30 || floor.transform.rotation.z < -0.30f)
            gameOver = true;
        if (gameOver)
            StartCoroutine(CollapseHouse());
        callEnemy += Time.deltaTime;
        //Debug.Log("callEnemy =" + callEnemy);
        if(callEnemy > enemyTimer && !gameOver)
        {
            callEnemy = 0f;
            if(enemyTimer > 2f)
                enemyTimer *= difficulty;
            WakeEnemy(Random.Range(0,3));
        }
    }

    public IEnumerator CollapseHouse()
    {
        leftBalance.gameObject.SetActive(false);
        rightBalance.gameObject.SetActive(false);
        gameOver = false;
        //Debug.Log("collapse house");
        Debug.Log("Game over");
        Rigidbody2D floorRb = floor.GetComponent<Rigidbody2D>();
        floorRb.constraints = RigidbodyConstraints2D.None;
        if (weightRight > weightLeft)
            floorRb.AddForce(new Vector2(9f, 9f));
        else
            floorRb.AddForce(new Vector2(-9f, 9f));
        yield return new WaitForSeconds(5f);
    }

    public void CalcRotation(float right, float left)
    {
        float diff = right - left;
        floor.transform.Rotate(new Vector3(0, 0, -damper * diff));
    }

    public void WakeEnemy(int enemy)
    {
        switch (enemy) { 
            case (0):
                WakeBat();
                break;
            case (1):
                WakeWitch();
                break;
            case (2):
                WakeReaper();
                break;
        }
    }

    public void WakeReaper()
    {
        reaper.SetActive(true);
        reaper.GetComponent<Reaper>().StartReaper();
    }
    public void WakeBat()
    {
        bat.SetActive(true);
        bat.GetComponent<Bat>().StartBat();
    }
    public void WakeWitch()
    {
        witch.SetActive(true);
        witch.GetComponent<Witch>().StartWitch();
    }




}
