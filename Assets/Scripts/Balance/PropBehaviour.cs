﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PropBehaviour : MonoBehaviour {
    public string nom;
    public float weight;
    public Sprite sprite;
    public float friction;
    public bool isSet;

    private void Start()
    {
        gameObject.name = nom;
        gameObject.GetComponent<Rigidbody2D>().mass = weight;
        gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        gameObject.GetComponent<BoxCollider2D>().sharedMaterial.friction = friction;
    }

    public void HammerProp()
    {
        

    }
}
