﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PropList : ScriptableObject{
    public List<PropDB> propList;
}
