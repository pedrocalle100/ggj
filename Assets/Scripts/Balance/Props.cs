﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Props : ScriptableObject {
    public string propName;
    public float weight;
    public Sprite sprite;
    public float friction;
    public bool isSet = false;
}
