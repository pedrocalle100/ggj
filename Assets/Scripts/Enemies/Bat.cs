﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Bat : MonoBehaviour
{

    public PropList propList;
    public GameObject blankProp;
    public GameObject propParent;

    public void StartBat()
    {
        StartCoroutine(BatBehaviour());
    }

    public IEnumerator BatBehaviour()
    {
        float randAnim = Random.Range(0.4f, AnimationLength("Move") -0.3f);
        
        yield return new WaitForSeconds(randAnim);
        Props selectedObject = ChooseProp(propList);
        InstantiateProp(selectedObject);
        yield return new WaitForSeconds(AnimationLength("Move"));
        gameObject.SetActive(false);
    }


    public float AnimationLength(string name)
    {
        Animator anim = gameObject.GetComponent<Animator>();
        float time = 0;

        RuntimeAnimatorController ac = anim.runtimeAnimatorController;

        for (int i = 0; i < ac.animationClips.Length; i++)
            if (ac.animationClips[i].name == name)
                time = ac.animationClips[i].length;


        Debug.Log(time);
        return time;
        
        
    }

    public Props ChooseProp(PropList list)
    {
        Props result = null;
        int rand = Random.Range(0, list.propList.Count);
        result = list.propList[rand].prop;
        return result;
    }

    public void InstantiateProp(Props prop)
    {
        Debug.Log("instantiated witch prop");
        GameObject newProp = blankProp;
        PropBehaviour propBehaviour = newProp.GetComponent<PropBehaviour>();

        propBehaviour.weight = prop.weight;
        propBehaviour.nom = prop.propName;
        propBehaviour.sprite = prop.sprite;
        propBehaviour.friction = prop.friction;
        propBehaviour.isSet = prop.isSet;

        
        Instantiate(newProp, transform.position, transform.rotation);
        //newProp.transform.SetParent(propParent.transform);
    }


}
