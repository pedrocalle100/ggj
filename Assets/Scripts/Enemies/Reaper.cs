﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reaper : MonoBehaviour
{
    public PropList propList;
    public float waitTime;
    public BoxCollider2D rightArea, leftArea;
    public GameObject blankProp;
    public GameObject propParent;

    public void StartReaper()
    {
        StartCoroutine(ReaperBehaviour());
    }

    public IEnumerator ReaperBehaviour()
    {
        
        int randPos = Random.Range(0, 2);
        Vector2 dropPos = Vector2.zero;
        if (randPos == 0)
            dropPos = ChooseDropPoint(rightArea.bounds);
        else
            dropPos = ChooseDropPoint(leftArea.bounds);
        transform.position = dropPos;
        yield return new WaitForSeconds(waitTime);
        Props selectedObject = ChooseProp(propList);
        InstantiateProp(selectedObject);
        yield return new WaitForSeconds(waitTime);
        gameObject.SetActive(false);
    }

    public static Vector2 ChooseDropPoint(Bounds area)
    {
        return new Vector2(
                Random.Range(area.min.x + 1f, area.max.x -1f),
                Random.Range(area.min.y + 8f, area.max.y)
            );
    }

    public Props ChooseProp(PropList list)
    {
        Props result = null;
        int rand = Random.Range(0, list.propList.Count);
        result = list.propList[rand].prop;
        return result;
    }

    public void InstantiateProp(Props prop)
    {
        GameObject newProp = blankProp;
        PropBehaviour propBehaviour = newProp.GetComponent<PropBehaviour>();

        propBehaviour.weight = prop.weight;
        propBehaviour.nom = prop.propName;
        propBehaviour.sprite = prop.sprite;
        propBehaviour.friction = prop.friction;

        Instantiate(newProp, transform.position, transform.rotation);
        //newProp.transform.SetParent(propParent.transform);
    }
}
