﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Witch : MonoBehaviour
{
    public PropList propList;
    public GameObject blankProp;
    public GameObject propParent;


    public void StartWitch()
    {
        StartCoroutine(WitchBehaviour());
    }
    public IEnumerator WitchBehaviour()
    {
        float randAnim = Random.Range(0.4f, AnimationLength("Move") - 0.3f);

        yield return new WaitForSeconds(randAnim);
        Props selectedObject = ChooseProp(propList);
        InstantiateProp(selectedObject);
        yield return new WaitForSeconds(AnimationLength("Move"));
        gameObject.SetActive(false);
    }


    public float AnimationLength(string name)
    {
        Animator anim = gameObject.GetComponent<Animator>();
        float time = 0;

        RuntimeAnimatorController ac = anim.runtimeAnimatorController;

        for (int i = 0; i < ac.animationClips.Length; i++)
            if (ac.animationClips[i].name == name)
                time = ac.animationClips[i].length;


        Debug.Log(time);
        return time;


    }

    public Props ChooseProp(PropList list)
    {
        Props result = null;
        int rand = Random.Range(0, list.propList.Count);
        result = list.propList[rand].prop;
        return result;
    }

    public void InstantiateProp(Props prop)
    {
        GameObject newProp = blankProp;
        PropBehaviour propBehaviour = newProp.GetComponent<PropBehaviour>();

        propBehaviour.weight = prop.weight;
        propBehaviour.nom = prop.propName;
        propBehaviour.sprite = prop.sprite;
        propBehaviour.friction = prop.friction;

        Instantiate(newProp, transform.position, transform.rotation);
        //newProp.transform.SetParent(propParent.transform);
    }

}
